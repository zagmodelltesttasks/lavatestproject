using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialiser : MonoBehaviour
{

    public static Player player;
    public static PlayerInput playerInput;
    public static Lights lights;
    public static UI_CanvasManager canvasManager;
    public static LevelManager levelManager;
    
    void Awake()
    {
        FindObjects();
        InitialiseObjects();
    }

    void FindObjects()
    {
        canvasManager = GameObject.Find("UI_Canvas").GetComponent<UI_CanvasManager>();
        player = GameObject.Find("Player").GetComponent<Player>();
        playerInput = player.transform.GetComponent<PlayerInput>();
        lights = GetComponent<Lights>();
        levelManager = GetComponent<LevelManager>();
    }

    void InitialiseObjects()
    {
        canvasManager.Initialise();
        player.Initialise();
        playerInput.Initialise();
        lights.Initialise();
        levelManager.Initialise();
    }
}
