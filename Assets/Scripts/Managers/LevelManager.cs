using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public static LevelManager levelManager;
    public List<Enemy> enemyList;
   
    public void Initialise()
    {
        levelManager = this;
        enemyList = new List<Enemy>();
    }

    public void AddEnemy(Enemy enemy)
    {
        enemyList.Add(enemy);
    }

    public void Remove_1_enemy(Enemy enemy)
    {
        enemyList.Remove(enemy);
        if (enemyList.Count == 0)
        {
            Initialiser.canvasManager.EnableRestartButton();
        }
    }

    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void SetPlayerDead()
    {
        foreach (Enemy enemy in enemyList)
        {
            enemy.SetEnemyIDle();
        }
    }

}
