﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour {
	public float lifeTimeFrom;
	public float lifeTimeTO;

	void Start () {
		Destroy (this.gameObject, Random.Range(lifeTimeFrom, lifeTimeTO));
	}
}
