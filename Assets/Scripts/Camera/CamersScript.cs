using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamersScript : MonoBehaviour
{
    private Vector3 SMDvelocity;

    void Update()
    {
        Vector3 playerPos = Initialiser.player.transform.position;
        Vector3 targetPos = new Vector3(playerPos.x -6f, this.transform.position.y, playerPos.z);
        this.transform.position = Vector3.SmoothDamp(this.transform.position, targetPos, ref SMDvelocity, 0.5f);
    }
}
