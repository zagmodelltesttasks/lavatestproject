using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.UI;

public class UI_CanvasManager : MonoBehaviour
{
    public UI_CanvasManager canvasManager;
    public GameObject ResetLevelbutton;
    private Animation ResetLevelbuttonAnim;
    private Text forceButtonText;
    private Text speedButtonText;
    private GameObject exitTowerButton;
    private Animation exitTowerButtonAnim;
    private GameObject MovePointArrow;
    private Animation MovePointArrowAnim;
    MeshRenderer [] movePointArrowRenderers;
    private float MovePointArrowSetUpTime;
    
    
    public void Initialise()
    {
        canvasManager = this;
        ResetLevelbutton = this.transform.Find("ResetButton").gameObject;
        ResetLevelbuttonAnim = ResetLevelbutton.GetComponent<Animation>();
        ResetLevelbutton.SetActive(false);
        forceButtonText = this.transform.Find("SwitchForceButton").GetComponentInChildren<Text>();
        speedButtonText = this.transform.Find("SwitchSpeedButton").GetComponentInChildren<Text>();
        exitTowerButton = this.transform.Find("ExitTowerButton").gameObject;
        exitTowerButtonAnim = exitTowerButton.GetComponent<Animation>();
        exitTowerButton.SetActive(false);
        MovePointArrow = GameObject.Find("BlueArrow").gameObject;
        MovePointArrowAnim = MovePointArrow.GetComponentInChildren<Animation>();
        MovePointArrowAnim["SetUp"].speed = 2f;
        movePointArrowRenderers = MovePointArrow.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mR in movePointArrowRenderers)
        {
            mR.enabled = false;
        }
    }

    void Update()
    {
        UpdateExitButtonPosition();
        UpdateMovePointArrow();
    }

    public void EnableRestartButton()
    {
        ResetLevelbutton.SetActive(true);
        ResetLevelbuttonAnim.Play("ResetButtonAppear_anim");
    }

    void Button_RestartLevel()
    {
        Initialiser.levelManager.RestartLevel();
    }

    public void SwitchForce(float force = 0)
    {
           force = Initialiser.player.SwitchForce();
           forceButtonText.text = "Force=" + force;
    }

    public void SwitchSpeed(float speed = 0)
    {
        speed = Initialiser.player.SwitchSpeed();
        speedButtonText.text = "Speed=" + speed ;
    }

    public void InitSetUpSpeedButton(float speed = 0)
    {
        speedButtonText.text = "Speed=" + speed ;
    }
    
    public void InitSetUpForceButton(float speed = 0)
    {
        forceButtonText.text = "Force=" + speed ;
    }

    public void ExitTowerButton()
    {
        Initialiser.player.ExitTower();
    }

    public void EnableExitButton(bool b)
    {
        exitTowerButtonAnim.Play("ResetButtonAppear_anim");
        exitTowerButton.SetActive(b);
    }

    void UpdateExitButtonPosition()
    {
        if (exitTowerButton.activeSelf && Initialiser.player.foundTower != null)
        {
            exitTowerButton.transform.position = Camera.main.WorldToScreenPoint(Initialiser.player.foundTower.DownPosition.transform.position);
        }
    }

    public void SetMovePointArrow(Vector3 position)
    {
        MovePointArrowSetUpTime = Time.time;
        foreach (MeshRenderer mR in movePointArrowRenderers)
        {
            mR.enabled = true;
        }
        MovePointArrow.transform.position = position;
        MovePointArrowAnim.Play("SetUp");
    }

    void UpdateMovePointArrow()
    {
        if (movePointArrowRenderers[0].enabled && Time.time >= MovePointArrowSetUpTime + 0.7f)
        {
            foreach (MeshRenderer mR in movePointArrowRenderers)
            {
                mR.enabled = false;
            }
        }
    }
}
