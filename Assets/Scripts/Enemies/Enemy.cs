using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : DestructableObject
{
    private GameObject player;

    private delegate void EnemyStateDelegate();

    private EnemyStateDelegate enemydelegate;

    private Animation enemyAnim;

    private Rigidbody[] rBodies;
    private Collider[] colliders;
    private Collider mainCharCollider;

    private bool isDead;
    private Gun enemyGun;
    private bool isPlayerSpotted;
    private float aimingTime = 1f;
    private float aimingStartTime;

    public enum StatesEnum
    {
        Idle,
        ReadyToFire,
        Fire,
    }

    private StatesEnum enemyStateEnum;

    void Start()
    {
        player = Initialiser.player.gameObject;

        enemyAnim = GetComponentInChildren<Animation>();
        rBodies = GetComponentsInChildren<Rigidbody>();
        mainCharCollider = GetComponent<Collider>();
        foreach (Rigidbody rbody in rBodies)
        {
            rbody.isKinematic = true;
        }

        colliders = transform.Find("RagdollModel").GetComponentsInChildren<Collider>();
        foreach (Collider col in colliders)
        {
            col.enabled = false;
        }

        enemyGun = this.transform
            .Find(
                "RagdollModel/SheKPelvis/SheKSpine1/SheKSpine2/SheKSpine3/SheKRibcage/SheKRArmCollarbone/SheKRArm1/SheKRArm2/SheKRArmPalm/Gun")
            .GetComponent<Gun>();
        enemyGun.InitialiseGun();
        Initialiser.levelManager.AddEnemy(this);
        SetEnemyIDle();
    }

    void EnemyIdle()
    {
        if (!Initialiser.player.isDead)
        {
            SearchPlayer();
        }
    }

    void EnemyReadyToFire()
    {
        SearchPlayer();
        RotateHorisontal();
        if (isPlayerSpotted)
        {
            if (Time.time >= aimingStartTime + aimingTime)
            {
                EnemyFire();
            }
        }
        else
        {
            SetEnemyIDle();
        }
    }

    void EnemyFire()
    {
        if (enemyStateEnum == StatesEnum.ReadyToFire)
        {
            if (isPlayerSpotted && !enemyAnim.IsPlaying("Fire") && !enemyAnim.IsPlaying("ReadyToFire") &&
                Time.time >= enemyGun.ShotStartTime + enemyGun.shotCooldown)
            {
                enemyAnim.Play("Fire");
                enemyGun.Fire(Initialiser.player.transform.position + new Vector3(0f, 0.5f, 0f));
            }
        }
    }

    void Update()
    {
        if (!isDead)
        {
            enemydelegate();
        }
    }

    void SearchPlayer()
    {
        isPlayerSpotted = false;
        RaycastHit hit;
        Vector3 startPoint = this.transform.position + new Vector3(0f, 1f, 0f);
        Vector3 target = Initialiser.player.transform.position + new Vector3(0f, 0.3f, 0f) - startPoint;
        Vector3 lowerTarget = Initialiser.player.transform.position - new Vector3(0f, 0f, 0f) - startPoint;
        if (Physics.Raycast(startPoint, target, out hit, 300f) && Physics.Raycast(startPoint, lowerTarget, out hit, 300f))
        {
            Debug.DrawLine(startPoint, Initialiser.player.transform.position );
            if (hit.transform.gameObject.layer == 6)
            {
                isPlayerSpotted = true;
                if (enemyStateEnum == StatesEnum.Idle)
                {
                    aimingStartTime = Time.time;
                    SetEnemyReadyToFire();
                }
            }
        }
    }

    void RotateHorisontal()
    {
        if (isPlayerSpotted)
        {
            Vector3 point = Initialiser.player.transform.position + new Vector3(0f, 0.5f, 0f);
            Vector3 lookPos = point - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            this.transform.rotation = rotation;
        }
    }


    public void SetEnemyIDle()
    {
        if (!enemyAnim.IsPlaying("Idle"))
        {
            enemyAnim.Play("Idle");
            enemydelegate = EnemyIdle;
            enemyStateEnum = StatesEnum.Idle;
        }
    }

    void SetEnemyReadyToFire()
    {
        if (!enemyAnim.IsPlaying("ReadyToFire"))
        {
            enemyAnim.Play("ReadyToFire");
            enemydelegate = EnemyReadyToFire;
            enemyStateEnum = StatesEnum.ReadyToFire;
        }
    }

    public void Death(Vector3 forceDirection, Vector3 hitPoint)
    {
        enemyAnim.Stop();
        mainCharCollider.enabled = false;
        float closestDistance = 100f;
        Rigidbody closestRigidbody = null;
        foreach (Rigidbody rbody in rBodies)
        {
            rbody.isKinematic = false;
            float distance = Vector3.Distance(rbody.transform.position, hitPoint);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestRigidbody = rbody;
            }
        }

        foreach (Collider col in colliders)
        {
            col.enabled = true;
        }

        if (closestRigidbody != null)
        {
            closestRigidbody.AddForce(forceDirection);
        }

        if (!isDead)
        {
            Initialiser.lights.SetLight(this.transform.position, Initialiser.lights.BlueLight);
        }

        Initialiser.levelManager.Remove_1_enemy(this);
        isDead = true;
    }

    override public void Damage(int damage, Vector3 forceDirection, Vector3 hitPOint)
    {
        HitPoints = HitPoints - damage;
        if (HitPoints <= 0)
        {
            Death(forceDirection, hitPOint);
        }
    }
    
    
}
