using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObject : MonoBehaviour
{
    public int maxHitPoints = 1;
    public int HitPoints;
    public GameObject destructionPrefab;

    void Start()
    {
        HitPoints = maxHitPoints;
    }

    public virtual void Damage(int damage, Vector3 forceDirection, Vector3 hitPOint)
    {
        HitPoints = HitPoints - damage;
        if (HitPoints <= 0)
        {
                if(destructionPrefab != null)
                {
                    GameObject destr = Instantiate(destructionPrefab, this.transform.position, this.transform.rotation) as GameObject;
                }
                Destroy(this.gameObject);
        }
    }


}
