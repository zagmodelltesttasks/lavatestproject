using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject UpPosition;
    public GameObject DownPosition;


    void Start()
    {
        UpPosition = this.transform.Find("UpPos").gameObject;
        DownPosition = this.transform.Find("DownPos").gameObject;
    }
}
