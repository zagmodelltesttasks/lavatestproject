using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GunData", menuName = "Gun Data", order = 51)]
public class GunData : ScriptableObject
{
    [SerializeField] public float shotCooldown_1;
    [SerializeField] public float shotCooldown_2;
    [SerializeField] public int damage;
    [SerializeField] public float force_1;
    [SerializeField] public float force_2;
    [SerializeField] public float force_3;
    [SerializeField] public float bulletSpeed;
    [SerializeField] public GameObject bulletModel;
}
