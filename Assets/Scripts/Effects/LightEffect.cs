using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightEffect : MonoBehaviour
{
    private bool isKillLightFading;
    private Light light;
    private float killLightFadingStartTime;
    
    public void Initialise()
    {
        light = GetComponent<Light>();
        light.enabled = false;
    }
  
    public void SetLightinPosition(Vector3 position)
    {
        light.enabled = true;
        light.intensity = 6f;
        light.transform.position = position + new Vector3(0f, 6f, 0f);
        isKillLightFading = true;
        killLightFadingStartTime = Time.time;
    }

    void Update()
    {
        if (isKillLightFading )
        {
            if (light.intensity >= 0 && Time.time >= killLightFadingStartTime + 0.2f)
            {
                light.intensity -= 0.2f;
            }
            if (light.enabled && light.intensity <= 0 )
            {
                light.enabled = false;
            }
        }
    }
}
