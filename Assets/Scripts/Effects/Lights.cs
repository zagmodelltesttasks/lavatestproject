using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    public static Lights lights;

    public LightEffect BlueLight;
    public LightEffect RedLight;
    
    public void Initialise()
    {
        lights = this;
        BlueLight = GameObject.Find("BlueLight").gameObject.GetComponent<LightEffect>();
        BlueLight.Initialise();
        RedLight= GameObject.Find("RedLight").gameObject.GetComponent<LightEffect>();
        RedLight.Initialise();
    }

    public void SetLight(Vector3 position, LightEffect recievedLight)
    {
        recievedLight.SetLightinPosition(position);
    }
}
