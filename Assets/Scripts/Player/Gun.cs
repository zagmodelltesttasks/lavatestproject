using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GunData gunData;
    public float ShotStartTime;
    private GameObject muzzlePoint;
    private ParticleSystem particleSystem;

    public float shotCooldown;
    public float force;

    private List<float> shotCooldowns;
    private List<float> forces;

    private int currentForceNumber =0;
    public void InitialiseGun()
    {
        ShotStartTime = Time.time;
        muzzlePoint = this.transform.Find("MuzzlePoint").gameObject;
        particleSystem = this.transform.Find("MuzzleParticles").GetComponent<ParticleSystem>();
       
        shotCooldowns = new List<float>();
        shotCooldowns.Add(gunData.shotCooldown_1);
        shotCooldowns.Add(gunData.shotCooldown_2);

        forces = new List<float>();
        forces.Add(gunData.force_1);
        forces.Add(gunData.force_2);
        forces.Add(gunData.force_3);
        
        SetShotCooldown(0);
        SetForce(0);
        Initialiser.canvasManager.InitSetUpForceButton(force);
    }

    public void Fire(Vector3 targetPos)
    {
        ShotStartTime = Time.time;
        SpawnBullet(targetPos);
        particleSystem.Play();
    }

    void SpawnBullet(Vector3 _targetpos)
    {
        GameObject bullet = Instantiate(gunData.bulletModel, muzzlePoint.transform.position, Quaternion.LookRotation(muzzlePoint.transform.forward));
        bullet.transform.LookAt(_targetpos);
        BulletScript bulletScript = bullet.GetComponent<BulletScript>();
        bulletScript.SetUpBulletStats(gunData.damage, force, gunData.bulletSpeed);
    }

    public void SetShotCooldown(int _cooldown)
    {
        shotCooldown = shotCooldowns[_cooldown];
    }

    public void SetForce(int _force)
    {
        force = forces[_force];
    }

    public void SwitchForce()
    {
        currentForceNumber++;
        if (currentForceNumber >= forces.Count())
        {
            currentForceNumber = 0;
        }
        force = forces[currentForceNumber];
    }
}
