using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Player : DestructableObject
{
    private NavMeshAgent navAgent;

    delegate void PlayerState();

    private PlayerState playerStateDelegate;
    private Vector3 movePos;
    private bool isMovingToTower;
    private bool isOnTower;

    private CapsuleCollider playerCollider;
    
    public enum statesEnum
    {
        Idle,
        Moving,
        ReadyToFire,
        Fire,
        Duck
    }

    public statesEnum state;
    private Animation playerAnim;
    private Transform dummy;

    private GameObject spineBone;
    public float angle;
    public float dotVector;

    private Gun gun;
    private GameObject muzzlePoint;

    public PlayerData playerData;
    private Rigidbody[] rBodies;
    private Collider[] colliders;
    public  bool isDead;
    private bool isRunning;

    public Tower foundTower;
    
    public void Initialise()
    {
        isMovingToTower = false;
        navAgent = this.gameObject.GetComponent<NavMeshAgent>();
        playerAnim = GetComponentInChildren<Animation>();
        spineBone = this.transform.Find("PlayerModel/SheKPelvis/SheKSpine1").gameObject;
        dummy = this.transform.Find("PlayerModel/SheKPelvis/dummy");
        gun = this.transform.Find("PlayerModel/SheKPelvis/SheKSpine1/SheKSpine2/SheKSpine3/SheKRibcage/SheKRArmCollarbone/SheKRArm1/SheKRArm2/SheKRArmPalm/Gun").GetComponent<Gun>();
        gun.InitialiseGun();
        playerCollider = GetComponent<CapsuleCollider>();

        rBodies = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rbody in rBodies)
        {
            rbody.isKinematic = true;
        }
        colliders = transform.Find("PlayerModel").GetComponentsInChildren<Collider>();
        foreach (Collider col in colliders)
        {
            col.enabled = false;
        }
        
        playerStateDelegate = Idle;
        playerAnim.Play("Idle");
        playerAnim["ReadyToFire"].speed = 1.5f;
        isRunning = false;
        navAgent.speed = playerData.playerSpeed_2;
        state = statesEnum.Idle;
        Initialiser.canvasManager.InitSetUpSpeedButton(playerData.playerSpeed_2);
        isRunning = true;
    }

    void Update()
    {
        if (!isDead)
        {
            playerStateDelegate();
        }
    }

    void LateUpdate()
    {
        if (isDead)
        {
            RotateTorsoToTarget();
        }
    }

    void Moving()
    {
        navAgent.SetDestination(movePos);
        if (Vector3.Distance(this.transform.position, movePos) <= 0.6f)
        {
            if (isMovingToTower)
            {
                Initialiser.canvasManager.EnableExitButton(true);
                SetReadyToFire();
            }
            else
            {
                if (state != statesEnum.Duck)
                {
                    SetIdle();
                }
            }
        }
    }

    void Idle()
    {

    }

    void ReadyToFire()
    {

    }

    public void MouseL_Click(RaycastHit hit)
    {
        if (state != statesEnum.Fire && state != statesEnum.ReadyToFire && state != statesEnum.Duck)
        {
            NavMeshHit navRaycastHit;
            if (NavMesh.SamplePosition(hit.point, out navRaycastHit, 1f, NavMesh.AllAreas))
            {
                if (hit.transform != null)
                {
                    foundTower = hit.transform.GetComponent<Tower>();
                    if (foundTower != null)
                    {
                        movePos = foundTower.UpPosition.transform.position;
                        isMovingToTower = true;
                    }
                    else
                    {
                        movePos = hit.point;
                        isMovingToTower = false;
                    }
                    Initialiser.canvasManager.SetMovePointArrow(movePos);
                    SetMoving();
                }
            }
        }
    }

    public void MouseFollow(Vector3 point)
    {
        RotateHorisontal(point);
    }

    void RotateHorisontal(Vector3 point)
    {
        if (point != Vector3.zero)
        {
            Vector3 lookPos = point - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            this.transform.rotation = rotation;
            movePos = point; 
        }
    }

    void RotateTorsoToTarget()
    {
        if (angle != 0)
        {
            angle = 0;
        }
        if (state == statesEnum.Fire || state == statesEnum.ReadyToFire)
        {
            if ( movePos != null)
            {
                Vector3 tempAngles = spineBone.transform.localEulerAngles;

                Vector3 targetVector = movePos - this.transform.position;
                angle = Vector3.Angle(targetVector, this.transform.forward);
                
                float dot = Vector3.Dot(this.transform.TransformDirection(Vector3.up), targetVector );
                if (dot < 0)
                {
                    angle = -angle;
                }

                if (angle < -25f)
                {
                    angle = -25;
                }

                spineBone.transform.localEulerAngles = new Vector3(tempAngles.x,tempAngles.y,angle -20f);
            }
        }
    }

    public void SetIdle()
    {
        if (isOnTower)
        {
              SetReadyToFire();
        }
        else
        {
            if (state != statesEnum.Idle)
            {
                navAgent.isStopped = false;
                navAgent.ResetPath();
                playerStateDelegate = Idle;
                playerAnim.Play("Idle");
                state = statesEnum.Idle;
            }
        }
    }

    void SetMoving()
    {
        if (state != statesEnum.Moving)
        {
            navAgent.isStopped = false;
            playerAnim.Stop();
            playerStateDelegate = Moving;
            if (isRunning)
            {
            playerAnim.Play("Run");
            }
            else
            {
                playerAnim.Play("Walk");
            }
            state = statesEnum.Moving;
        }
    }

    void SetReadyToFire(bool isNoAnim = false)
    {
        if (state != statesEnum.ReadyToFire)
        {
            playerStateDelegate = ReadyToFire;
            if (!isNoAnim)
            {
                playerAnim.Play("ReadyToFire");
            }

            isOnTower = true;
            state = statesEnum.ReadyToFire;
        }
    }

    public void SetDuck()
    {
        if (state != statesEnum.Duck)
        {
            navAgent.isStopped = true;
            if (!playerAnim.IsPlaying("Duck"))
            {
                playerAnim.Play("Duck");  
            }
            state = statesEnum.Duck;
            SetColliderDuck();
        }
    }

    public void SetGetUp()
    {
        SetColliderStanding();
        SetIdle();
    }
    
    void SetColliderDuck()
    {
        playerCollider.height = 0.77f;
           playerCollider.center = new Vector3(0f, -0.14f, 0f);
    }

    void SetColliderStanding()
    {
        playerCollider.height = 1.59f;
        playerCollider.center = new Vector3(0f, 0.34f, 0f);
    }


    public void Fire()
    {
        if (state == statesEnum.ReadyToFire && state != statesEnum.Duck)
        {
            if (!playerAnim.IsPlaying("Fire") && !playerAnim.IsPlaying("ReadyToFire")&& Time.time >= gun.ShotStartTime + gun.shotCooldown)
            {
                playerAnim.Play("Fire");
                gun.Fire(movePos);
            }
        }
    }

    public void ExitTower()
    {
        Initialiser.canvasManager.EnableExitButton(false);
        isOnTower = false;
        isMovingToTower = false;
        movePos = foundTower.DownPosition.transform.position;
        SetMoving();
    }

    override public void Damage(int damage, Vector3 forceDirection, Vector3 hitPOint)
    {
        Death(forceDirection, hitPOint);
    }
    
    void Death(Vector3 forceDirection, Vector3 hitPoint)
    {
        playerAnim.Stop();
        playerCollider.enabled = false;
        float closestDistance = 100f;
        Rigidbody closestRigidbody = null;
        foreach (Rigidbody rbody in rBodies)
        {
            rbody.isKinematic = false;
            float distance = Vector3.Distance(rbody.transform.position, hitPoint);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestRigidbody = rbody;
            }
        }

        foreach (Collider col in colliders)
        {
            col.enabled = true;
        }

        if (closestRigidbody != null)
        {
            closestRigidbody.AddForce(forceDirection);
        }

        if (!isDead)
        {
            Initialiser.lights.SetLight(this.transform.position, Initialiser.lights.RedLight);
        }
        Initialiser.canvasManager.EnableExitButton(false);
        isDead = true;
        Initialiser.levelManager.SetPlayerDead();
        Initialiser.canvasManager.EnableRestartButton();
    }

    public float SwitchForce()
    {
        gun.SwitchForce();
        return gun.force;
    }

    public float SwitchSpeed()
    {
        if (!isRunning)
        {
            isRunning = true;
            navAgent.speed = playerData.playerSpeed_2;
            if (state == statesEnum.Moving)
            {
                playerAnim.Play("Run");
            }

            return playerData.playerSpeed_2;
        }
        else
        {
            isRunning = false;
            navAgent.speed = playerData.playerSpeed_1;
            if (state == statesEnum.Moving)
            {
                playerAnim.Play("Walk");
            }

            return playerData.playerSpeed_1;
        }
    }

}
