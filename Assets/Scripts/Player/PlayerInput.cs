using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public static PlayerInput input;

    public void Initialise()
    {
        input = this;
    }

    void Update()
    {
        if (!Initialiser.player.isDead)
        {
            MouseL_Click();
            MouseFollow();
            MouseHold();
            HoldR_Click();
            R_MouseUp();
        }
    }

    void MouseHold()
    {
        if (Input.GetMouseButton(0) && Initialiser.player.state == Player.statesEnum.ReadyToFire ||
            Initialiser.player.state == Player.statesEnum.Fire)
        {
            MouseFollow();
            Initialiser.player.Fire();
        }
    }

    void MouseFollow()
    {
        if (Initialiser.player.state == Player.statesEnum.ReadyToFire ||
            Initialiser.player.state == Player.statesEnum.Fire)
        {
            Ray camRay = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(camRay, out hit);
            Initialiser.player.MouseFollow(hit.point);
        }
    }

    void HoldR_Click()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Initialiser.player.SetDuck();  
        }
    }

    void R_MouseUp()
    {
        if (Input.GetMouseButtonUp(1))
        {
            Initialiser.player.SetGetUp();
        }
    }

    void MouseL_Click()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray camRay = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(camRay, out hit);
            if (hit.point != null)
            {
                Initialiser.player.MouseL_Click(hit);
            }
        }
    }
}
