using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private float force;
    private int damage;
    private float bulletSpeed;
    public GameObject destructionPrefab;
    private SphereCollider collider;


    public void SetUpBulletStats(int _damage, float _force, float _bulletSpeed)
    {
        damage = _damage;
        force = _force;
        bulletSpeed = _bulletSpeed;
        collider = GetComponent<SphereCollider>();
        
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 0.5f))
        {
            Rigidbody rigidBody = hit.transform.GetComponent<Rigidbody>();
            if (rigidBody != null)
            {
                rigidBody.AddForce(this.transform.forward * force);
            }
            DestructableObject destrObj = hit.transform.GetComponentInParent<DestructableObject>();
            
            if (destrObj != null)
            {
                destrObj.Damage(damage, this.transform.forward * force, hit.point );
            }
  
            if (destructionPrefab != null)
            {
                  GameObject destrPrefab = Instantiate(destructionPrefab, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
            }
            GameObject trailObj = this.transform.Find("Trail").gameObject;
            if (trailObj != null)
            {
                trailObj.transform.parent = null;
                trailObj.AddComponent<Destroy>();
                Destroy destr = trailObj.GetComponent<Destroy>();
                destr.lifeTimeFrom = 2f;
                destr.lifeTimeTO = 2f;

            }
            Destroy(this.gameObject);
        }
    }
}
